from collections import namedtuple
import os
from typing import List, Tuple

import click
from PIL import Image,ImageFilter, ImageDraw

DEFAULT_CORDS = {3:{'center':322, 'heigh':528,'rows':1,'columns':3},
                 1:{'center':0, 'rows':1,'columns':1},
                 2:{'center':0,'rows':1,'columns':2},
                 4:{'center':0,'rows':2,'columns':2},
                 6:{'center':0,'rows':2,'columns':3},
                 }


PX_HEIGH = 529

def sort_by_creation_time(file_list: List[str]) -> List[str]:
    new_file_list: List[Tuple[float, str]] = []
    for f in file_list:
        creation_time = os.path.getmtime(f)
        new_file_list.append((creation_time,f))
    sorted_file_list = [f[1] for f in sorted(new_file_list, key=lambda x:x[0])]
    return sorted_file_list

def calc_output_image_width(heigh, hw_ratio=33.87/14):
    return int(heigh * hw_ratio)

def calc_frame_image_width(width, count=3):
    return int(width / count)

def get_frames_cords(image_heigh, image_width, frame_heigh, frame_width, columns, rows, heigh_center_correction = 0,width_correction=0):
    frame_cords = []
    CropCords = namedtuple('Cords', ['x_start', 'y_start', 'x_finish', 'y_fnish'])
    for r in range(1, rows+1):
        for c in range(1, columns+1):
            center_vertical = round(r*image_heigh/rows - image_heigh/rows/2)
            center_horizontal = round(c*image_width/columns - image_width/columns/2)
            frame_cords.append(CropCords(center_horizontal - frame_width // 2 + width_correction,
                                         center_vertical - frame_heigh // 2 + heigh_center_correction,
                                         center_horizontal + frame_width // 2 + width_correction,
                                         center_vertical + frame_heigh // 2 + heigh_center_correction,
                                         ))
    print(frame_cords)
    return frame_cords






@click.command()
@click.option('--viewer', default='philips', help='source viewer')
@click.option('--count', default=3, help='number of images')
@click.option('--hw_ratio', default=254/140, help='ratio of width to heigh')
@click.option('--heigh_px', default=529, help='frame heigh in px')
@click.option('--output_path', default=None, help='output path')
@click.option('--median_filter', default=None, help='denoise with median filter')
@click.option('--width_correction', default=0, help='denoise with median filter')
@click.option('--heigh_correction', default=0, help='denoise with median filter')
@click.option('--sort', default=True, help='sort with file creation time')
@click.argument('path')
def cropit(path,output_path,hw_ratio,count,heigh_px,viewer,median_filter,width_correction,heigh_correction, sort):
    if output_path is None:
        output_path=os.path.join(path,"crop")
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    high_center = DEFAULT_CORDS[count]['center']
    file_list = [os.path.join(path,f) for f in os.listdir(path)]
    print(sort)
    if sort:
        file_list = sort_by_creation_time(file_list)
    for num,f in enumerate(file_list):
        try:
            im = Image.open(os.path.join(path,f))
        except:
            continue

        if high_center==0:
            high_center=im.size[1]//2
        new_image_width = calc_output_image_width(heigh=heigh_px,hw_ratio=hw_ratio)
        frame_width = calc_frame_image_width(new_image_width,count)
        new_im = Image.new('RGB',(new_image_width, heigh_px))
        heigh_correction = DEFAULT_CORDS[count].get('heigh_correction') or heigh_correction or high_center-im.size[1]//2
        crop_cords = get_frames_cords(image_heigh=im.size[1],
                                      image_width=im.size[0],
                                      frame_heigh=heigh_px,
                                      frame_width=frame_width,
                                      rows=DEFAULT_CORDS[count]['rows'],
                                      columns=DEFAULT_CORDS[count]['columns'],
                                      heigh_center_correction=heigh_correction,
                                      width_correction=width_correction
                                      )
        #img1 = ImageDraw.Draw(im)


        for frame_num, cords in enumerate(crop_cords):
            croped_pic = im.crop(cords)
            #img1.rectangle(cords, fill="#ffff33", outline="red")
            if median_filter is not None:
                croped_pic=croped_pic.filter(ImageFilter.MedianFilter(int(median_filter)))
            new_im.paste(croped_pic, (frame_num*frame_width, 0))
        #im.show()
        new_file_name = f"{str(num).zfill(3)}.jpeg"
        new_im = new_im.resize((round(PX_HEIGH*hw_ratio),PX_HEIGH),resample = Image.LANCZOS)
        new_im.save(os.path.join(output_path,new_file_name))

if __name__ == '__main__':
    cropit()
